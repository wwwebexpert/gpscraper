from django.urls import path
from . import views
from GPSpider.views import HomeView, AppSearchView, AppDetailView, Results, AboutView

app_name = "GPSpider"


urlpatterns = [

    path('', views.HomeView, name='home'),

    path('about', views.AboutView, name='about'),

    path('app/search/', AppSearchView.as_view(), name='app_search'),

    path('app/results/', views.Results, name='results'),

    path('app/detail/<uid>/', AppDetailView.as_view(), name='app_detail'),

]

