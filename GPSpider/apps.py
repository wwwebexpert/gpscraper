from django.apps import AppConfig


class GpspiderConfig(AppConfig):
    name = 'GPSpider'
