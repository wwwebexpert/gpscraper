import requests
from django.shortcuts import render
from django.views.generic.edit import FormView
from django.views.generic import View
from GPSpider.models import AppData, AppSearchIndex
from GPSpider.forms import SearchForm
from GPSpider.helper import PlayStoreHelper
from django.db.models import Q


# def Landing(request):
#     template_name="GPSpider/landing.html"
#     return render(request,template_name)

def AboutView(request):
    template_path = 'GPSpider/about.html'
    return render(request, template_path)

def HomeView(request):
    ayo = "Google Play Store Scraper"
    context = { 'ayo':ayo}
    template_path = 'GPSpider/home.html'
    return render(request, template_path, context)



class AppSearchView(FormView):

    template_name = 'GPSpider/search.html'
    form_class = SearchForm

    def form_valid(self, form):
        query = form.cleaned_data.get('query', None)
        print(query)
        result = PlayStoreHelper.search(query)
        print(result)
        context = {'form': self.form_class, 'result': result}
        return render(self.request, self.template_name, context)


class AppDetailView(View):

    template_name = 'GPSpider/detail.html'

    def get(self, request, uid):
        app_data = PlayStoreHelper.get_app_details(uid)
        return render(self.request, self.template_name, app_data)



def Results(request):

    result = AppData.objects.all()
    query = request.GET.get("q")
    if query:
        result = result.filter(
            Q(name__icontains=query)|
            Q(uid__icontains=query)|
            Q(dev_name__icontains=query)


        ).distinct()
    return render(request, 'GPSpider/resultlist.html', {'result': result,})
